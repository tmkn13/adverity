import { test, expect, Page } from "@playwright/test";

test.beforeEach(async ({ page }) => {
    await page.goto("http://localhost:4173/");
});

//Example E2E Test
test.describe("Campaign Viewer Tests", () => {
    test("should render main view", async ({ page }) => {
        await expect(page.locator("data-testid=viz")).toBeVisible();
    });
});
