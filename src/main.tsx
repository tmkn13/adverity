import React from "react";
import ReactDOM from "react-dom";

import { App } from "./App";
import { LocalDataLoader } from "./data/Data";

import "./index.css";

ReactDOM.render(
    <React.StrictMode>
        <App loader={new LocalDataLoader()} />
    </React.StrictMode>,
    document.getElementById("root")
);
