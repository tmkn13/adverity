import { FC, useState, useEffect } from "react";

import { CampaignData, CampaignMap, IDataLoader, mapDTO } from "./data/Data";
import { ISelectOption, Select } from "./components/Select/Select";
import { LoadingIndicator } from "./components/LoadingIndicator/LoadingIndicator";
import { ErrorMessage } from "./components/ErrorMessage/ErrorMessage";
import { CampaignChart } from "./components/CampaignChart/CampaignChart";

import "./App.css";

//wrapper component for the App
const AppContainer: React.FC = ({ children }) => <div className="App">{children}</div>;

interface IAppProps {
    loader: IDataLoader; //easily switch where to load the data from, via import, serverless function, server etc
}

export const App: FC<IAppProps> = ({ loader }) => {
    const [isLoading, setIsLoading] = useState<boolean>(true);
    const [isError, setIsError] = useState<boolean>(false);

    //campaign data will be stored here
    const [campaignMap, setCampaignMap] = useState<CampaignMap>(new Map());

    //currently selected datasource
    const [dataSource, setDataSource] = useState<string | undefined>(undefined);

    //currently selected campaign
    const [campaign, setCampaign] = useState<string | undefined>(undefined);

    //load data upon first render
    useEffect(() => {
        (async () => {
            try {
                const data = await loader.getData();
                const campaignMap = mapDTO(data);

                setCampaignMap(campaignMap);
            } catch {
                setIsError(true);
            } finally {
                setIsLoading(false);
            }
        })();
    }, []);

    if (isLoading)
        return (
            <AppContainer>
                <LoadingIndicator />
            </AppContainer>
        );

    if (isError)
        return (
            <AppContainer>
                <ErrorMessage />
            </AppContainer>
        );

    return (
        <AppContainer>
            <h1>Campaign Viewer</h1>
            <div className="container">
                <div className="settings">
                    <Select
                        name="DataSource"
                        selectName="datasource"
                        selectedValue={dataSource}
                        selectOptions={getDataSourceOptions(campaignMap)}
                        onSelect={value => {
                            setDataSource(value);
                            setCampaign(undefined);
                        }}
                    />
                    <Select
                        name="Campaign"
                        selectName="campaign"
                        selectedValue={campaign}
                        selectOptions={getCampaignOptions(dataSource, campaignMap)}
                        onSelect={value => setCampaign(value)}
                    />
                </div>
                <div className="main">
                    <CampaignChart
                        dataSource={dataSource}
                        campaign={campaign}
                        campaignData={getCampaignData(dataSource, campaign, campaignMap)}
                    />
                </div>
            </div>
        </AppContainer>
    );
};

//create options for the datasource select
function getDataSourceOptions(campaignMap: CampaignMap): ISelectOption[] {
    const options: ISelectOption[] = [];

    try {
        [...campaignMap.keys()].forEach(dataSource => {
            options.push({
                value: dataSource,
                label: dataSource
            });
        });
    } catch {
    } finally {
        return options;
    }
}

//create options for the campaign select
function getCampaignOptions(
    dataSource: string | undefined,
    campaignMap: CampaignMap
): ISelectOption[] {
    const options: ISelectOption[] = [];
    try {
        if (typeof dataSource === "undefined") throw new Error(`dataSource was undefined`);

        const campaigns = campaignMap.get(dataSource);

        if (typeof campaigns === "undefined") throw new Error(`Couldn't get campaigns`);

        [...campaigns.keys()].forEach(dataSource => {
            options.push({
                value: dataSource,
                label: dataSource
            });
        });
    } catch {
    } finally {
        return options;
    }
}

//helper function to get campaign data based on selected "dataSource" and "campaign"
function getCampaignData(
    dataSource: string | undefined,
    campaign: string | undefined,
    campaignMap: CampaignMap
): CampaignData {
    const campaignData: CampaignData = [];

    try {
        if (typeof dataSource === "undefined" || typeof campaign === "undefined")
            throw new Error(`Received undefined data`);

        const data = campaignMap.get(dataSource)?.get(campaign) ?? [];

        campaignData.push(...data);
    } catch {
    } finally {
        return campaignData;
    }
}
