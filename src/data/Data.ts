// hypothetical DTO, this should be the response that comes from the server
export interface IDataDTO {
    data: DataRowDTO[];
}

interface DataRowDTO {
    date: string;
    dataSource: string;
    campaign: string;
    clicks: number;
    impressions: number;
}

//the DTO will be mapped into this format
export type CampaignMap = Map<string /* data source */, Campaigns>;

type Campaigns = Map<string /* campaign name */, CampaignData>;
export type CampaignData = ICampaignData[];

interface ICampaignData {
    date: string;
    clicks: number;
    impressions: number;
}

//Interface that is reponsible to load the data
export interface IDataLoader {
    getData: () => Promise<IDataDTO>;
}

//DataLoader used in the app
//For simplicity reasons it loads the data via a dynamic js import
export class LocalDataLoader implements IDataLoader {
    //gets and converts csv data
    //for a production app I would use a battle tested csv lib instead since it's actually a complicated format
    async getData(): Promise<IDataDTO> {
        //get data
        const { default: rawData } = await import("./data.csv?raw");
        //convert data into rows, disregard first row since it's the header
        const [header, ...rows] = rawData
            .split("\n")
            .map(line => line.trim())
            .filter(line => line !== ``);

        //will hold final data
        const data: DataRowDTO[] = [];

        for (const row of rows) {
            try {
                //split row into individual chunks
                const [date, dataSource, campaign, clicks, impressions] = row
                    .split(`,`)
                    .filter(entry => entry.trim() !== ``);

                //make sure we have all the chunks, disregard incomplete chunks
                if (
                    typeof date !== "undefined" &&
                    typeof dataSource !== "undefined" &&
                    typeof campaign !== "undefined" &&
                    typeof clicks !== "undefined" &&
                    typeof impressions !== "undefined"
                ) {
                    //convert chunks into DataRowDTO
                    data.push({
                        date,
                        dataSource,
                        campaign,
                        clicks: parseInt(clicks),
                        impressions: parseInt(impressions)
                    });
                }
            } catch (e) {
                console.error(e);
            }
        }

        return {
            data
        };
    }
}

//helper function to map the DTO to the required format
export function mapDTO({ data }: IDataDTO): CampaignMap {
    const campaignMap: CampaignMap = new Map();

    for (const { date, dataSource, campaign, clicks, impressions } of data) {
        //get the all campaigns for a datasource, create a new one if not yet present
        let campaigns: Campaigns = campaignMap.get(dataSource) ?? new Map();
        //get campaign data, create a new one if not yet present
        let singleCampaignData: ICampaignData[] = campaigns.get(campaign) ?? [];

        //add campaign data
        singleCampaignData.push({
            date,
            clicks,
            impressions
        });

        //set data for single campaign
        campaigns.set(campaign, singleCampaignData);
        //set campaigns for datasource
        campaignMap.set(dataSource, campaigns);
    }

    return campaignMap;
}
