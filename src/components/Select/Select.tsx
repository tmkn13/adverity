import React from "react";

export interface ISelectOption {
    value: string;
    label: string;
}

interface ISelectProps {
    name: string; //heading
    selectName: string; //name for the selectbox
    selectOptions: ISelectOption[]; //options for the selectbox
    selectedValue: string | undefined; //option that should be selected upon rendering
    onSelect: (value: string) => void; //callback that is triggered upon selecting an option
}

export const Select: React.FC<ISelectProps> = ({
    name,
    selectName,
    selectOptions,
    onSelect,
    selectedValue
}) => {
    return (
        <>
            <h2>{name}</h2>
            <select
                name={selectName}
                onChange={event => onSelect(event.target.value)}
                value={selectedValue ?? "pristine"}
            >
                <option value={"pristine"} disabled>{`Select a ${name}`}</option>
                {selectOptions.map(({ label, value }) => (
                    <option key={value} value={value}>
                        {label}
                    </option>
                ))}
            </select>
        </>
    );
};
