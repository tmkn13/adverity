import React from "react";

import style from "./LoadingIndicator.module.css";

export const LoadingIndicator: React.FC = () => {
    return (
        <div className={style.container}>
            <div className={style.spinner}>
                <div className={style.doublebounce1}></div>
                <div className={style.doublebounce2}></div>
            </div>
        </div>
    );
};
