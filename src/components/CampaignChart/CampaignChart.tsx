import React from "react";
import {
    LineChart,
    Line,
    XAxis,
    YAxis,
    CartesianGrid,
    Tooltip,
    Legend,
    ResponsiveContainer
} from "recharts";

import styles from "./CampaignChart.module.css";

import { CampaignData } from "../../data/Data";

const CampaignChartContainer: React.FC = ({ children }) => (
    <div className={styles.container} data-testid="viz">
        {children}
    </div>
);

interface ICampaignChartProps {
    dataSource: string | undefined;
    campaign: string | undefined;
    campaignData: CampaignData;
}

//component that vizualises the campaign data
export const CampaignChart: React.FC<ICampaignChartProps> = ({
    dataSource,
    campaign,
    campaignData
}) => {
    //inform the user that he needs to select a "dataSource" and "campaign"
    if (typeof dataSource === "undefined" && typeof campaign === "undefined")
        return (
            <CampaignChartContainer>
                <span>
                    Please select a <b>datasource</b> and a <b>campaign</b>
                </span>
            </CampaignChartContainer>
        );

    if (typeof dataSource !== "undefined" && typeof campaign === "undefined")
        return (
            <CampaignChartContainer>
                <span>
                    Please select a campaign for datasource <b>{`${dataSource}`}</b>
                </span>
            </CampaignChartContainer>
        );

    //render chart
    return (
        <CampaignChartContainer>
            <ResponsiveContainer width="100%" height="100%">
                <LineChart
                    width={500}
                    height={600}
                    data={campaignData}
                    margin={{
                        top: 5,
                        right: 30,
                        left: 20,
                        bottom: 5
                    }}
                >
                    <CartesianGrid strokeDasharray="3 3" />
                    <XAxis dataKey="date" />
                    <YAxis yAxisId="left" label={{ value: "Impressions", angle: -90, dx: 40 }} />
                    <YAxis
                        yAxisId="right"
                        orientation="right"
                        label={{ value: "Clicks", angle: -90, dx: -40 }}
                    />
                    <Tooltip />
                    <Legend />
                    <Line
                        yAxisId="left"
                        type="monotone"
                        dataKey="impressions"
                        stroke="#8884d8"
                        activeDot={{ r: 8 }}
                    />
                    <Line yAxisId="right" type="monotone" dataKey="clicks" stroke="#82ca9d" />
                </LineChart>
            </ResponsiveContainer>
        </CampaignChartContainer>
    );
};
