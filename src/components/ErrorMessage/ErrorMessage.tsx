import React from "react";

import styles from "./ErrorMessage.module.css";

export const ErrorMessage: React.FC = () => {
    return (
        <div className={styles.container} data-testid="error">
            Whoops something went wrong!
        </div>
    );
};
