import { describe, it, expect } from "vitest";
import { render } from "@testing-library/react";

import { App } from "./App";
import { IDataDTO, IDataLoader, LocalDataLoader } from "./data/Data";

//Example Unit Test
describe("<App /> Tests", () => {
    it("Renders <App /> sucessfully", async () => {
        const { findByTestId } = render(<App loader={new LocalDataLoader()} />);

        expect(await findByTestId("viz", undefined, { timeout: 10000 })).not.toBe(null);
    });

    it("Renders <ErrorMessage /> on data error", async () => {
        const ErrorDataLoader = new (class implements IDataLoader {
            async getData(): Promise<IDataDTO> {
                throw new Error();
            }
        })();

        const { findByTestId } = render(<App loader={ErrorDataLoader} />);

        expect(await findByTestId("error")).not.toBe(null);
    });
});
