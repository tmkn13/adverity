# Campaign Viewer
![Desktop View](/desktop.png "Desktop View")

## Responsive
The application was built with responsiveness in mind. It uses a _mobile first_ approach.

Here's the mobile view:
![Mobile View](/mobile.png "Mobile View")

## Install
It was developed with the latest (currently available) Node.js LTS.

1. checkout repo
2. run `npm run install` to install dependencies
3. run `npm run dev` to start the development server

## Technologies
I opted to use [Vite](https://vitejs.dev/) instead of _React Create App_ for scaffolding the application. Because it's a relatively new addition to the ecosystem (by the creator of Vue) and it is praised for it's speed, also I wanted to try it out.

The application utilizes _React_ with _TypeScript_. Styling was done via normal CSS (no 3rd party framework).

### React
The whole application uses `FunctionComponents` throughout. No class based components have been used. Additionally no dedicated state management library was used since the example was basic enough to do without.

Also `Context` wasn't needed. The application passes props down to the appropriate components. Since there is no deep nesting, `Context` didn't turn out to be needed.

### TypeScript
The whole application uses the `strict` type setting enabling a high degree of type safety. Additionally [`noUncheckedIndexedAccess`](https://www.typescriptlang.org/tsconfig#noUncheckedIndexedAccess) was enabled to improve the type safety when working with Arrays.

### Recharts
[Recharts](https://recharts.org/en-US) was used to draw the vizualisations.

### Prettier
[Prettier](https://prettier.io/) was used to ensure the same source code format across the whole code base.

## Data Loading
The input data was in `CSV`. Although I wouldn't recommend it in a production app, I wrote my own code to parse the data and turn it into a hypothetical `DTO`. If this should go productive I would look for a battle tested `CSV` lib as `CSV` parsing is hard!

Also I took the liberty to disregard entries who did not have entries for both `clicks` and `impressions`.

To keep the data loading flexible I created a `IDataLoader` interface. It's purpose is to load the `DTO`. Could be an endpoint or in this case, via a dynamic JS import to keep things simple.
```typescript
export interface IDataLoader {
    getData: () => Promise<IDataDTO>;
}
```
The `loader` is passed as a prop to the `<App />` to make it flexible from where data should be loaded:
```typescript
<App loader={new LocalDataLoader()} />
```

## Components
All the components can be found in [`./src/components`](./src/components/).

### `<App />`
[`./src/App.tsx`](./src/App.tsx)

The `<App />` component loads the data via a `loader` prop. While it's loading it displays a `<LoadingIndicator />`.

Once data is loaded it is then transformed in a `useEffect` hook. If there is an error, the `<ErrorMessage />` component is used.

If data mapping was successfull, the `DataSource` and `Campaing` selects will be populated.

Once a `DataSource` and `Campaign` was selected, the vizualisation will be drawn via the `<CampaignChart />` component.

### `<Select />`
[`./src/components/Select/Select.tsx`](./src/components/Select/Select.tsx)

The `<Select />` is used to select a _DataSource_ and their respective _Campaign_.

### `<CampaignChart />`
[`./src/components/CampaignChart/CampaignChart.tsx`](./src/components/CampaignChart/CampaignChart.tsx)

The `<CampaignChart />` component uses `Recharts` to display the vizualisation of the _clicks_ and _impressions_ for a campaign.

It gets the currently selected `DataSource`, `Campaign` alongside the actual data needed to display the vizualisation via props.

Even though not necessary for the vizualisation, `DataSource` and `Campaign` are used to display helpful information for when the user has not yet selected a `DataSource` or `Campaign`.

### `<LoadingIndicator />`
[`./src/components/LoadingIndicator/LoadingIndicator.tsx`](./src/components/LoadingIndicator/LoadingIndicator.tsx)

Since data loading is asyncronous a progress indicator will be shown to the user.

### `<ErrorMessage />`
[`./src/components/ErrorMessage/ErrorMessage.tsx`](./src/components/ErrorMessage/ErrorMessage.tsx)

If during the `CSV` data parsing and mapping stage something goes wrong the `<ErrorMessage />` component will be used to communicate the error to the user.

## Testing
The App is also fully setup for Unit and E2E tests. 

Note: due to time constraints only the setup was done with some minimal actual tests.

### Unit Tests
Unit tests are done via [`Vitest`](https://vitest.dev/). The companion test framework for Vite.

In conjunction with [`React Testing Library`](https://testing-library.com/docs/react-testing-library/intro/);

#### Example Test
[`./src/App.test.tsx`](./src/App.test.tsx)

#### Run
```
npm run test
```

#### Code Coverage
To generate code coverage run
```
npm run coverage
```

### E2E Tests
E2E Tests are done via [`Playwright`](https://playwright.dev/).

The tests are run on
- Chrome
- Firefox
- Webkit

#### Example Test
[`./e2e/example.spec.ts`](./e2e/example.spec.ts)

#### Run
1. Build project
```
npm run build
```

2. Start server
```
npm run preview
```

3. To start the E2E tests, in another terminal run:
```
npm run e2e
```

## Other
If this was a production app I would also setup CI/CD so that it builds the App and runs the tests automatically upon PR creation. I would also add static code analysis like `SonarQube`.

Even though the App was built with responsiveness in mind, some small visual glitches like resizing the window doesn't resize the chart remain, due to time constraints.